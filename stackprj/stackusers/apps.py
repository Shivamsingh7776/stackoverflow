from django.apps import AppConfig


class StackusersConfig(AppConfig):
    
    name = 'stackusers'

    def ready(self):
        import stackusers.signals
